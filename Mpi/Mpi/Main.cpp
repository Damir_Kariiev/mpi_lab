#define _USE_MATH_DEFINES

#include "mpi.h"
#include <cmath>
#include <windows.h>
#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>


using namespace std;

string ReadFile(const string& FilePath) {
    fstream in(FilePath);
    if (in.is_open())
    {
        ostringstream Stream;
        Stream << in.rdbuf();
        return Stream.str();
    }
    else
    {
        return {};
    }
}

int GetWordCount(char* Fragment, int FragmentCharAmount, char* WordIter, int StringCharAmount)
{
    int count = 0;
    int globalposition = 0;

    for (globalposition = 0; globalposition < FragmentCharAmount - StringCharAmount + 1; ++globalposition)
    {

        int localposition = 0;

        while (localposition != StringCharAmount &&
            toupper(*(WordIter + localposition)) == toupper(*(Fragment + globalposition + localposition)))
        {
            ++localposition;
        }

        count += (localposition == StringCharAmount);
    }

    return count;
}


int main(int argc, char* argv[])
{
    int Result;
    double startwtime;
    double endwtime;

    MPI_Init(&argc, &argv);

    int CurrentId{};
    int NumProcesses{};

    MPI_Comm_size(MPI_COMM_WORLD, &NumProcesses);
    MPI_Comm_rank(MPI_COMM_WORLD, &CurrentId);

    string Text;
    string Word = argv[1];

    MPI_Status status;
    int value = 0;

    if (CurrentId == 0)
    {
        Text = ReadFile("input.txt");

        value = Text.length() / NumProcesses;
    }
    
    if (CurrentId == 0) {
        startwtime = MPI_Wtime();
    }


    MPI_Bcast(&value, 1, MPI_INT, 0, MPI_COMM_WORLD);
    

    char* workText = new char[value + 1];
    if (CurrentId == 0) {
        for (size_t i = 1; i < NumProcesses; i++)
        {
            MPI_Send(Text.c_str() + value * i, value, MPI_CHAR, i, 0, MPI_COMM_WORLD);
        }
        workText = Text.data();
    }
    else
    {
        MPI_Recv(workText, value, MPI_CHAR, 0, 0, MPI_COMM_WORLD, &status);
    }

    
    int counter = GetWordCount(workText, value, Word.data(), Word.length());
    cout << "Process with id: " << CurrentId << " Find: " << Word << " is " << counter << endl;


    MPI_Reduce(&counter, &Result, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (CurrentId == 0)
    {
        endwtime = MPI_Wtime();
        std::cout << "Actual: " << Result << '\n';
        cout.precision(6);
        std::cout << "Duration: " << (endwtime - startwtime) * 1000 << " ms\n";
    }

    MPI_Finalize();
    return 0;
}